package Matrix;

import java.text.DecimalFormat;

public class Matrix {
	private double[] A;
	private double[] B;
	private double[] CT;
	private double b;
	private double c;
	
	public Matrix(double[] A, double[] B, double[] CT) {
		this.A = A;
		this.B = B;
		this.CT = CT;
	}
	
	public double landa1() {
		double a = 1;
		double b = -(A[0]+A[3]);
		double c = (A[0] * A[3]) - (A[1] * A[2]);
		double l1 = ((-b)+Math.sqrt(b*b-(4*a*c)))/(2*a);
		DecimalFormat df = new DecimalFormat("###.####");
		this.b = b;
		this.c = c;
		return l1;
	}
	
	public double landa2() {
		double a = 1;
		double b = -(A[0]+A[3]);
		double c = (A[0] * A[3]) - (A[1] * A[2]);
		double l2 = ((-b)-Math.sqrt(b*b-(4*a*c)))/(2*a);
		return l2;
	}
	
	public double[] Lagrange1() {
		double l1 = landa1();
		double l2 = landa2();
		double[] L = new double[4];
		L[0] = (A[0] - l2)/(l1-l2);
		L[1] = A[1]/(l1-l2);
		L[2] = A[2]/(l1-l2);
		L[3] = (A[3] - l2)/(l1-l2);
		DecimalFormat df = new DecimalFormat("###.####");
		System.out.println("landa1(c) = " + df.format(l1) + " landa2(f) = " + df.format(l2) + "\n");
		System.out.println("L1 = " + df.format(L[0]) + " " + df.format(L[1]) + "\n     " 
		+ df.format(L[2]) + " " + df.format(L[3]) + "\n");
		return L;
	}
	public double[] Lagrange2() {
		double l1 = landa1();
		double l2 = landa2();
		double[] L = new double[4];
		L[0] = (A[0] - l1)/(l2-l1);
		L[1] = A[1]/(l2-l1);
		L[2] = A[2]/(l2-l1);
		L[3] = (A[3] - l1)/(l2-l1);
		DecimalFormat df = new DecimalFormat("###.####");
		System.out.println("L2 = " + df.format(L[0]) + " " + df.format(L[1]) + "\n     " 
		+ df.format(L[2]) + " " + df.format(L[3]) + "\n");
		return L;
	}
	
	public double M1() {
		double[] L1 = Lagrange1();
		double M1 = 0;
		M1 = ((CT[0] * L1[0] + CT[1] * L1[2]) * B[0]) + ((CT[0] * L1[1] + CT[1] * L1[3]) * B[1]);
		DecimalFormat df = new DecimalFormat("###.####");
		System.out.printf("M1(b) = " + df.format(M1) + "\n\n");
		return M1;
	}
	
	public double M2() {
		double[] L2 = Lagrange2();
		double M2 = 0;
		M2 = ((CT[0] * L2[0] + CT[1] * L2[2]) * B[0]) + ((CT[0] * L2[1] + CT[1] * L2[3]) * B[1]);
		DecimalFormat df = new DecimalFormat("###.####");
		System.out.printf("M2(d) = " + df.format(M2) + "\n\n");
		return M2;
	}
	public void calc(){
		DecimalFormat df = new DecimalFormat("###.####");
		double M1 = M1();
		double M2 = M2();
		System.out.println("landa^1 =  " + df.format(this.b) + " landa^0 = " + df.format(this.c) + "\n");
		System.out.println("Rendszer Valasza:\n h= D*d[k] + E[k-1](" + df.format(M1) 
		+ "" + df.format(this.landa1()) +"^(k-1) + " 
		+ df.format(M2) + "" + df.format(this.landa2()) + "^(k-1))");
	}
}
